
# Marshmallow Practice

Marshmallow schemas can be used to validate input data, deserialize input data to app-level objects and serialize app-level objects to primitive Python types. 
The serialized objects can then be rendered to standard formats such as JSON for use in an HTTP API.


## Acknowledgements

 - [Marshmallow Documentation](https://marshmallow.readthedocs.io/en/stable/quickstart.html#declaring-schemas)
 

## API Reference

#### POST User Information

```http
  POST /create
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `name` | `string` | **Required**. Maximum Length = 20|
| `age` | `int` | **Required**. Age less than 80|
| `email` | `string` | **Required**.|
| `phone` | `string` | **Required**. Length must be 10.|
| `address` | `string` | **Optional**.|


## Authors

- [@ujjawalpoudel](https://gitlab.com/ujjawalpoudel)


## License

[MIT](https://choosealicense.com/licenses/mit/)


## Tech Stack


**Server:** Python

