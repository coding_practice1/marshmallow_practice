from db_connection import db
from models import User


def user_create(data):
    """This function is used to create a new user.

    Args:
        data (dict): A dictionary of user information

    Returns:
        bool: True/False
        str: Appropriate Message after success or failure operation.
    """
    try:
        User().load(data)
        db.users.insert_one(data)

        status = True
        msg = "User created successfully"
    except Exception as e:
        msg = str(e)
        status = False
    return {"status": status, "msg": msg}
