import os
from pymongo import MongoClient
from dotenv import load_dotenv

load_dotenv()

user = os.getenv("DB_USER")
password = os.getenv("DB_PASSWORD")
database_name = os.getenv("DATABASE_NAME")
host = os.getenv("DB_HOST")

host = f"mongodb+srv://{user}:{password}@{host}/{database_name}?retryWrites=true&w=majority"

client = MongoClient(host)
db = client[database_name]
