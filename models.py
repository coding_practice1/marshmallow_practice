from marshmallow import Schema, fields, post_load, ValidationError, validates, validate


class User(Schema):
    name = fields.String(validate=validate.Length(max=20), required=True)
    age = fields.Integer(required=True)
    email = fields.Email(required=True)
    phone = fields.String(validate=validate.Length(equal=10), required=True)
    address = fields.String()

    @validates("age")
    def validate_age(self, age):
        if age > 80:
            raise ValidationError("The age is too old!")
